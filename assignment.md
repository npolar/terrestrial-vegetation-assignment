# Assignment
Submit documentation and skeleton of your project code that will illustrate your answer to the questions below. It is up to you to decide, how much or how little to submit, this material will be used during the interview process.
We expect you to submit the results either by sharing link to your code repository (e.g.on gitlab.com or github.com), or submit it as a zip archive.

# 1. Retrieval methodology
Describe process workflow that will lead to creating a dataset describing terrestrial vegetation based on satellite observations.
Which tools and data collections will allow efficient generation of the final product? What are the challenges?
Submit code that will illustrate your approach.

# 2. Dataset generation
Describe preparation of the dataset ready for reuse and publication.
What properties of the dataset will make it compliant with [FAIR](https://www.go-fair.org/fair-principles/) guidelines?

